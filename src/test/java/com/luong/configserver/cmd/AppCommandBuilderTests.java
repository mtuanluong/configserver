package com.luong.configserver.cmd;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Paths;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.luong.configserver.ConfigserverApplicationTests;
import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.config.model.ArtifactType;
import com.luong.configserver.user.action.UserActionManager;
import com.luong.configserver.user.request.CommandRequest;
import com.luong.configserver.user.request.model.AngularAppArtifactCharacter;
import com.luong.configserver.user.request.model.Command;
import com.luong.configserver.user.request.model.GradleProjArtifactCharacter;
import com.luong.configserver.user.request.model.SpringAppArtifactCharacter;
import com.luong.configserver.user.response.CommandResponse;

public class AppCommandBuilderTests extends ConfigserverApplicationTests {

	private @Autowired UserActionManager userActionManager;

	// @formatter:off
	@Test
	public void angularAppCommandBuilderTest() {
		CommandRequest commandRequest = new CommandRequest(
			new Artifact("carmanui", Paths.get("C:/Software/Blah.jar"), ArtifactType.Frontend),
			Command.Run,
			AngularAppArtifactCharacter.builder()
    			.port(4208)
    			.useHttpServer("dist/carmanui")
    			.prod(true)
    			.openInBrowser(true)
    			.build());

		assertThat(userActionManager.handle(commandRequest)).satisfies(responseOpt -> {
			String commandStr = responseOpt
				.map(CommandResponse.class::cast)
				.map(CommandResponse::getCommand)
				.get();
			assertThat(commandStr).isEqualTo("http-server dist/carmanui --prod --port 4208 -o");
		});
	}
	
	@Test
	public void gradleProjCommandBuilderTest() {
		CommandRequest commandRequest = new CommandRequest(
			new Artifact("carman", Paths.get("C:/Software/Blah.jar"), ArtifactType.Backend),
			Command.Build,
			GradleProjArtifactCharacter.builder()
    			.build());
		assertThat(userActionManager.handle(commandRequest)).satisfies(responseOpt -> {
			String commandStr = responseOpt
				.map(CommandResponse.class::cast)
				.map(CommandResponse::getCommand)
				.get();
			assertThat(commandStr).isEqualTo("./gradlew build -x test");
		});
	}
	
	@Test
	public void springAppCommandBuilderTest() {
		CommandRequest commandRequest = new CommandRequest(
				new Artifact("carman", Paths.get("C:/Software/Blah.jar"), ArtifactType.Backend),
				Command.Run,
				SpringAppArtifactCharacter.builder()
					.springProfile("staging")
					.build());
		assertThat(userActionManager.handle(commandRequest)).satisfies(responseOpt -> {
			String commandStr = responseOpt
					.map(CommandResponse.class::cast)
					.map(CommandResponse::getCommand)
					.get();
			assertThat(commandStr).isEqualTo("java -jar -Dspring.profiles.active=staging \"C:\\Software\\test.jar\"");
		});
	}
    // @formatter:on

}
