package com.luong.configserver.config;

import static com.luong.configserver.config.model.ArtifactType.Backend;
import static com.luong.configserver.config.model.ArtifactType.Frontend;
import static com.luong.configserver.config.model.ArtifactType.Library;
import static com.luong.configserver.config.model.ArtifactType.Repository;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.luong.configserver.config.model.ApplicationConfig;
import com.luong.configserver.config.model.Artifact;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties("project")
public class ProjectConfig implements ApplicationConfig {

	private @Setter Map<String, Path> backends = new HashMap<>();
	private @Setter Map<String, Path> frontends = new HashMap<>();
	private @Setter Map<String, Path> libraries = new HashMap<>();
	private @Setter Map<String, Path> repositories = new HashMap<>();
	private @Getter Set<Artifact> allArtifacts;

	// @formatter:off
	@PostConstruct
	public void init() {
		allArtifacts = Stream.of(
			Pair.of(backends, Backend),
			Pair.of(frontends, Frontend),
			Pair.of(libraries, Library),
			Pair.of(repositories, Repository)
		)
		.map(p -> Pair.of(p.getLeft().entrySet(), p.getRight()))
		.flatMap(p -> p.getLeft().stream().map(e -> new Artifact(e.getKey(), e.getValue(), p.getRight())))
		.collect(Collectors.toSet());
	}
    // @formatter:on

}
