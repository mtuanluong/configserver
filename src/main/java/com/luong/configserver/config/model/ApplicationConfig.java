package com.luong.configserver.config.model;

import java.util.Set;

public interface ApplicationConfig {

	Set<Artifact> getAllArtifacts();

}
