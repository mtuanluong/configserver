package com.luong.configserver.config.model;

import java.nio.file.Path;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Artifact {

	private String name;
	
	private Path path;

	private ArtifactType artifactType;

}
