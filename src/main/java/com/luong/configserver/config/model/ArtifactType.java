package com.luong.configserver.config.model;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ArtifactType {
	// @formatter:off

	Backend("backend"),
	Folder("folder"),
	Frontend("frontend"),
	JarsExecutable("jar"),
	JavaHome("java"),
	Library("library"),
	Repository("repository"),
	WindowsExecutable("executable"),
	Webpage("webpage"),
	Undefined("undefinded");
	
	private final @NonNull @Getter String value;
	
	public static Optional<ArtifactType> find(String str) {
		return Stream.of(ArtifactType.values())
			.filter(a -> a.getValue().equals(str))
			.findFirst();
	}

	public static List<String> findAllValues() {
		return Stream.of(ArtifactType.values())
    		.map(a -> a.getValue())
    		.sorted()
    		.collect(Collectors.toList());
	}

    // @formatter:on
}
