package com.luong.configserver.config;

import static com.luong.configserver.config.model.ArtifactType.WindowsExecutable;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.luong.configserver.config.model.ApplicationConfig;
import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.config.model.ArtifactType;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties("executable")
public class ExecutableConfig implements ApplicationConfig {

	private @Setter Map<String, Path> windows = new HashMap<>();
	private @Setter Map<String, Path> jars = new HashMap<>();
	private @Getter Set<Artifact> allArtifacts;

	// @formatter:off
	@PostConstruct
	public void postConstruct() {
		allArtifacts = Stream.of(
			Pair.of(windows, WindowsExecutable),
			Pair.of(jars, ArtifactType.JarsExecutable)
		)
		.map(p -> Pair.of(p.getLeft().entrySet(), p.getRight()))
		.flatMap(p -> p.getLeft().stream().map(e -> new Artifact(e.getKey(), e.getValue(), p.getRight())))
		.collect(Collectors.toSet());
	}
	// @formatter:on
}
