package com.luong.configserver.config;

import static com.luong.common.functional.MessageFunctions.DUPLICATE_KEYS_NOT_PERMITED;
import static com.luong.common.path.PathFormat.Windows;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.luong.common.path.PathFormat;
import com.luong.common.path.PathService;
import com.luong.configserver.config.model.ApplicationConfig;
import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.config.model.ArtifactType;

@Component
public class ApplicationConfiguration {

	private @Autowired Set<ApplicationConfig> applicationConfigs;
	private @Autowired PathService pathService;

	// @formatter:off
	public Optional<Artifact> findArtifact(String name) {
		return applicationConfigs.stream()
			.map(ApplicationConfig::getAllArtifacts)
			.flatMap(Collection::stream)
			.filter(a -> a.getName().equals(name))
			.findFirst();
	}

	public Set<Artifact> findArtifacts(Optional<ArtifactType> artifactTypeOpt) {
		return applicationConfigs.stream()
			.map(ApplicationConfig::getAllArtifacts)
			.flatMap(Collection::stream)
			.filter(a -> artifactTypeOpt
				.map(a.getArtifactType()::equals)
				.orElse(true))
			.collect(Collectors.toSet());
	}

	public Set<Artifact> findArtifacts(ArtifactType appType) {
		return findArtifacts(Optional.of(appType));
	}

	public Map<String, String> findPaths(Optional<ArtifactType> artifactTypeOpt, Optional<PathFormat> pathFormatOpt) {
		return findArtifacts(artifactTypeOpt)
			.stream()
			.map(a -> Pair.of(a.getName(), pathService.convert(
				a.getPath(), pathFormatOpt.orElse(Windows), false)))
			.sorted((p1, p2) -> p1.getLeft().compareTo(p2.getLeft()))
			.collect(Collectors.toMap(
				Pair::getLeft, 
				Pair::getRight,
                DUPLICATE_KEYS_NOT_PERMITED,
                TreeMap::new));
	}
    // @formatter:on

}
