package com.luong.configserver.context.provider;

public interface DataGather {

	void gatherBackendInfo();

	void gatherFrontendInfo();

	void gatherLibraryInfo();

	void gatherOtherInfo();

	// @formatter:off
	default void run() {
		gatherBackendInfo();
		gatherFrontendInfo();
		gatherLibraryInfo();
		gatherOtherInfo();
	}
    // @formatter:on

}
