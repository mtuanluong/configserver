package com.luong.configserver.context.provider;

import static com.luong.configserver.config.model.ArtifactType.Backend;
import static com.luong.configserver.config.model.ArtifactType.Library;

import java.nio.file.Paths;
import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.luong.configserver.config.ApplicationConfiguration;
import com.luong.configserver.context.ContextDataService;
import com.luong.configserver.context.model.FrontendProjectInfo;
import com.luong.configserver.context.model.GradleProjInfo;

@Component("initialDataGather")
public class InitialDataGather implements DataGather {

	private @Autowired ContextDataService contextDataService;
	private @Autowired ApplicationConfiguration configuration;

	private final String CURRENT_PROJECT_VERSION = "0.0.1-SNAPSHOT";

	// @formatter:off
	@Override
	public void gatherBackendInfo() {
		configuration.findArtifacts(Backend)
			.forEach(a -> contextDataService.getAllBackends().put(a, 
				GradleProjInfo.builder().name(a.getName()).version(CURRENT_PROJECT_VERSION).build()));
		
		Stream.of(
			Pair.of("carman", "carmanagement"),
			Pair.of("manspace", "manspmanager"),
			Pair.of("mobridge", "mobilebridge"),
			Pair.of("objgen", "objgenerator"),
			Pair.of("procman", "processmanager")
		)
			.forEach(p -> contextDataService.getAllBackends().put(
				configuration.findArtifact(p.getLeft()).get(), 
				GradleProjInfo.builder().name(p.getRight()).version(CURRENT_PROJECT_VERSION).build()));
		
		contextDataService.getAllBackends().put(
			configuration.findArtifact("dataservice").get(), 
			GradleProjInfo.builder()
				.name("dataservice")
				.version("1.0-SNAPSHOT").build());
	}

	@Override
	public void gatherFrontendInfo() {
		Stream.of(
			// <Alias in configuration, actual app name configured somewhere in application, port>
			Triple.of("carmanui", "carmanagement-ui", 4201),
			//Triple.of("gatecontrolui", "gatecontrolui", 4202),
			//Triple.of("phadminui", "phadminui", 4203),
			//Triple.of("manspaceui", "manspaceui", 4205),
			//Triple.of("pushnotifyui", "pushnotifyui", 4206),
			Triple.of("objgenui", "objgenui", 4207),
			Triple.of("mobridgeui", "mobridgeui", 4208),
			Triple.of("procmanui", "procmanui", 4000)
		)
			.forEach(t -> contextDataService.getAllFrontends().put(
				configuration.findArtifact(t.getLeft()).get(), 
				FrontendProjectInfo.builder()
					.port(t.getRight())
					.distFolderRelPath(Paths.get("dist", t.getMiddle()))
					.build()));
	}

	@Override
	public void gatherLibraryInfo() {
		configuration.findArtifacts(Library)
			.forEach(a -> contextDataService.getAllLibraries().put(a, 
					GradleProjInfo.builder().name(a.getName()).version(CURRENT_PROJECT_VERSION).build()));
	}

	@Override
	public void gatherOtherInfo() {
		contextDataService.getOperatingSystemName().set("windows");
	}
    // @formatter:on

}
