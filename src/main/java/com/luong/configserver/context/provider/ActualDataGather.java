package com.luong.configserver.context.provider;

import static com.luong.common.functional.MessageFunctions.PRINT_DATA_GATHERED_INFO;
import static com.luong.configserver.config.model.ArtifactType.Backend;
import static com.luong.configserver.config.model.ArtifactType.Frontend;
import static com.luong.configserver.config.model.ArtifactType.Library;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.luong.configserver.config.ApplicationConfiguration;
import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.context.ContextDataService;
import com.luong.configserver.context.model.FrontendProjectInfo;
import com.luong.configserver.context.model.GradleProjInfo;
import com.luong.configserver.process.ProcessService;

import lombok.SneakyThrows;

@Component("actualDataGather")
public class ActualDataGather implements DataGather {

	private @Autowired ContextDataService contextDataService;
	private @Autowired ProcessService processService;
	private @Autowired ApplicationConfiguration configuration;

	// @formatter:off
	@Override
	@SneakyThrows
	public void gatherBackendInfo() {
		configuration.findArtifacts(Backend).stream()
			.map(a -> Pair.of(a, retrieveBackend(a)))
			.forEach(p -> {
				PRINT_DATA_GATHERED_INFO.accept(p.getLeft(), p.getRight());
				contextDataService.getAllBackends().put(p.getLeft(), p.getRight());
			});
	}

	@Override
	public void gatherFrontendInfo() {
		configuration.findArtifacts(Frontend).stream()
			.map(pr -> Pair.of(pr, retrieveFrontend(pr)))
			.forEach(p -> {
				if (p.getRight() != null) {
					PRINT_DATA_GATHERED_INFO.accept(p.getLeft(), p.getRight());
					contextDataService.getAllFrontends().put(p.getLeft(), p.getRight());
				}
		});
	}

	@Override
	public void gatherLibraryInfo() {
		configuration.findArtifacts(Library).stream()
    		.map(pr -> Pair.of(pr, retrieveBackend(pr)))
    		.forEach(p -> {
    			PRINT_DATA_GATHERED_INFO.accept(p.getLeft(), p.getRight());
    			contextDataService.getAllLibraries().put(p.getLeft(), p.getRight());
    		});
	}

	@Override
	public void gatherOtherInfo() {
		String osName = System.getProperty("os.name");
		PRINT_DATA_GATHERED_INFO.accept("SystemOS", osName);
		contextDataService.getOperatingSystemName().set(osName);
	}

	@SneakyThrows
	private GradleProjInfo retrieveBackend(Artifact artifact) {
		return processService.retrieveGradleProjInfo(artifact).get();
	}
	
	@SneakyThrows
	private FrontendProjectInfo retrieveFrontend(Artifact artifact) {
		return processService.retrieveFrontendProjInfo(artifact).get();
	}
    // @formatter:on

}
