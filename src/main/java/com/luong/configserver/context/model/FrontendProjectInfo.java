package com.luong.configserver.context.model;

import java.nio.file.Path;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
@Getter
public class FrontendProjectInfo {

	private @NonNull Integer port;

	private @NonNull Path distFolderRelPath;

}
