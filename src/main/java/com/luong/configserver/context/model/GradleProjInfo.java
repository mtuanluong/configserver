package com.luong.configserver.context.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Builder
@Getter
@ToString
public class GradleProjInfo {

	private @NonNull String name;

	private @NonNull String version;

}
