package com.luong.configserver.context;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;

import org.springframework.stereotype.Service;

import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.context.model.FrontendProjectInfo;
import com.luong.configserver.context.model.GradleProjInfo;

import lombok.Getter;

/**
 * Since getting the relevant data for gradle and angular project is time
 * extensive, it should be collected before and cached here
 */
@Service
public class ContextDataService {

	private @Getter Map<Artifact, FrontendProjectInfo> allFrontends = new ConcurrentHashMap<>();
	private @Getter Map<Artifact, GradleProjInfo> allBackends = new ConcurrentHashMap<>();
	private @Getter Map<Artifact, GradleProjInfo> allLibraries = new ConcurrentHashMap<>();
	private @Getter AtomicReference<String> operatingSystemName = new AtomicReference<>();

	// @formatter:off
	public Optional<FrontendProjectInfo> findFrontendInfo(Artifact project) {
		return Optional.ofNullable(allFrontends.get(project));
	}

	public Optional<GradleProjInfo> findBackendInfo(Artifact project) {
		return Optional.ofNullable(allBackends.get(project));
	}
	
	public Optional<GradleProjInfo> findLibraryInfo(Artifact project) {
		return Optional.ofNullable(allLibraries.get(project));
	}
    // @formatter:on

}
