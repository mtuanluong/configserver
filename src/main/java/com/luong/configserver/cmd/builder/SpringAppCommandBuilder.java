package com.luong.configserver.cmd.builder;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.luong.common.path.PathService;
import com.luong.configserver.context.ContextDataService;
import com.luong.configserver.user.request.CommandRequest;
import com.luong.configserver.user.request.model.SpringAppArtifactCharacter;

import lombok.Getter;

@Component
public class SpringAppCommandBuilder extends CmdCommandBuilder<SpringAppArtifactCharacter> {

	private @Getter final Class<SpringAppArtifactCharacter> type = SpringAppArtifactCharacter.class;

	private @Autowired ContextDataService contextDataService;
	private @Autowired PathService pathService;

	// @formatter:off
	@Override
	public String commandRun(CommandRequest commandRequest) {
		
		return Optional.of(type.cast(commandRequest.getArtifactCharacter()))
			.map(c -> {
				String jarFileStr = Optional.of(commandRequest)
					.map(CommandRequest::getArtifact)
					.flatMap(contextDataService::findBackendInfo)
					.map(info -> commandRequest.getArtifact().getPath()
						.resolve("build")
						.resolve("libs")
						.resolve(String.format("%s-%s.jar", info.getName(), info.getVersion())))
					.map(p -> pathService.convert(p, c.getPathFormat(), c.isQuotePath()))
					.orElseThrow(() -> new RuntimeException("Cannot get the needed info for command run"));
				
				String result = "java -jar";
				result = appendParam(result, "-Dspring.profiles.active=%s", Optional.ofNullable(c.getSpringProfile()));
				result = appendParam(result, "%s", Optional.of(jarFileStr));
				Optional<Object> otherArgs = c.getOtherSpringArgs().entrySet()
					.stream()
					.map(e -> String.format("--%s=%s", e.getKey(), e.getValue()))
					.reduce((str1, str2) -> String.format("%s %s", str1, str2))
					.map(Object.class::cast);

				return appendParam(result, "%s", otherArgs);
			})
			.get();
	}
    // @formatter:on

}
