package com.luong.configserver.cmd.builder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.luong.common.path.PathService;
import com.luong.configserver.user.request.CommandRequest;
import com.luong.configserver.user.request.model.WindowsExecutableArtifactCharacter;

import lombok.Getter;

@Component
public class WindowsExecutableCommand extends CmdCommandBuilder<WindowsExecutableArtifactCharacter> {

	private @Getter final Class<WindowsExecutableArtifactCharacter> type = WindowsExecutableArtifactCharacter.class;

	private @Autowired PathService pathService;

	// @formatter:off
	public String commandRun(CommandRequest commandRequest) {
		return pathService.convert(
			commandRequest.getArtifact().getPath(), 
			type.cast(commandRequest.getArtifactCharacter()).getPathFormat(), 
			type.cast(commandRequest.getArtifactCharacter()).isQuotePath());
	}
    // @formatter:on

}
