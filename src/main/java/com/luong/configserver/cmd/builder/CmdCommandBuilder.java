package com.luong.configserver.cmd.builder;

import static com.luong.common.functional.MessageFunctions.UNSUPPORTED_OPERATION;

import java.util.Optional;

import com.luong.configserver.user.request.CommandRequest;
import com.luong.configserver.user.request.model.ArtifactCharacter;

public abstract class CmdCommandBuilder<T extends ArtifactCharacter> {

	public abstract Class<T> getType();

	// @formatter:off
	public String commandBuild(CommandRequest commandRequest) {
		throw new UnsupportedOperationException(
			UNSUPPORTED_OPERATION.apply(commandRequest.getCommand(), commandRequest.getArtifact()));
	}

	public String commandRun(CommandRequest commandRequest) {
		throw new UnsupportedOperationException(
			UNSUPPORTED_OPERATION.apply(commandRequest.getCommand(), commandRequest.getArtifact()));
	}

	public String commandProperties(CommandRequest commandRequest) {
		throw new UnsupportedOperationException(
			UNSUPPORTED_OPERATION.apply(commandRequest.getCommand(), commandRequest.getArtifact()));
	}
	
	protected String appendOnCondition(String input, String append, boolean condition) {
		return condition ? 
			String.format("%s %s", input, append) :
			input;
	}

	protected String appendParam(String input, String strTemplate, Optional<Object> param) {
		return param
			.map(p -> String.format("%s %s", input, String.format(strTemplate, p)))
			.orElse(input);
	}
    // @formatter:on

}
