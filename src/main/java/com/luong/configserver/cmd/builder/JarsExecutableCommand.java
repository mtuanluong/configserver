package com.luong.configserver.cmd.builder;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.luong.common.path.PathService;
import com.luong.configserver.user.request.CommandRequest;
import com.luong.configserver.user.request.model.JarsExecutableArtifactCharacter;

import lombok.Getter;

@Component
public class JarsExecutableCommand extends CmdCommandBuilder<JarsExecutableArtifactCharacter> {

	private @Getter final Class<JarsExecutableArtifactCharacter> type = JarsExecutableArtifactCharacter.class;

	private @Autowired PathService pathService;

	// @formatter:off
	public String commandRun(CommandRequest commandRequest) {
		String result = "java -jar";
		result = appendParam(result, "%s", Optional.of(buildArtifactPath(commandRequest)));
		result = appendParam(result, "%s", Optional.of("delombok src -d src-delomboked"));
		
		return result;
	}
	
	private String buildArtifactPath(CommandRequest commandRequest) {
		return pathService.convert(
			commandRequest.getArtifact().getPath(), 
			type.cast(commandRequest.getArtifactCharacter()).getPathFormat(), 
			type.cast(commandRequest.getArtifactCharacter()).isQuotePath());
	}
    // @formatter:on

}
