package com.luong.configserver.cmd.builder;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.luong.configserver.user.request.CommandRequest;
import com.luong.configserver.user.request.model.AngularAppArtifactCharacter;

import lombok.Getter;

@Component
public class AngularAppCommandBuilder extends CmdCommandBuilder<AngularAppArtifactCharacter> {

	private @Getter final Class<AngularAppArtifactCharacter> type = AngularAppArtifactCharacter.class;

	// @formatter:off
	@Override
	public String commandBuild(CommandRequest commandRequest) {
		return Optional.of(type.cast(commandRequest.getArtifactCharacter()))
			.map(c -> {
				return appendOnCondition("ng build", "--prod", c.isProd());
			})
			.get();
	}
	
	@Override
	public String commandRun(CommandRequest commandRequest) {
		return Optional.of(type.cast(commandRequest.getArtifactCharacter()))
			.map(c -> {
				String result = c.getUseHttpServer() == null ? 
					"ng serve" :
					String.format("http-server %s", c.getUseHttpServer());
				result = appendOnCondition(result, "--prod", c.isProd());
				result = appendParam(result, "--port %s", Optional.ofNullable(c.getPort()));
				result = appendOnCondition(result, "-o", c.isOpenInBrowser());
				
				return result;
			})
			.get();
	}
    // @formatter:on

}
