package com.luong.configserver.cmd.builder;

import static com.luong.common.path.PathFormat.Cygwin;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.luong.configserver.user.request.CommandRequest;
import com.luong.configserver.user.request.model.GradleProjArtifactCharacter;

import lombok.Getter;

@Component
public class GradleProjCommandBuilder extends CmdCommandBuilder<GradleProjArtifactCharacter> {

	private @Getter final Class<GradleProjArtifactCharacter> type = GradleProjArtifactCharacter.class;

	// @formatter:off
	@Override
	public String commandBuild(CommandRequest commandRequest) {
		return Optional.of(type.cast(commandRequest.getArtifactCharacter()))
			.map(c -> {
				String result = c.isUseWrapper() ?
					Cygwin.equals(c.getPathFormat()) ? "./gradlew" : "gradlew" :
					"gradle";
				result = String.format("%s %s", result, "build");
				result = appendOnCondition(result, "-x test", c.isSkipTest());
				result = appendOnCondition(result, "publishToMavenLocal --continuous", c.isPublish());
				
				return result;
			})
			.get();
	}

	@Override
	public String commandProperties(CommandRequest commandRequest) {
		return Optional.of(type.cast(commandRequest.getArtifactCharacter()))
				.map(c -> {
					String result = c.isUseWrapper() ?
							Cygwin.equals(c.getPathFormat()) ? "./gradlew" : "gradlew" :
								"gradle";
					result = String.format("%s %s", result, "properties");
					
					return result;
				})
				.get();
	}
	// @formatter:on

}
