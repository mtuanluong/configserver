package com.luong.configserver.converter;

import static com.luong.common.functional.MessageFunctions.NO_TYPE_FOUND;

import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.luong.common.path.PathFormat;

@Component
@ConfigurationPropertiesBinding
public class StringToPathFormatConverter implements Converter<String, PathFormat> {

	// @formatter:off
	@Override
	public PathFormat convert(String source) {
		return PathFormat.find(source)
			.orElseThrow(() -> new RuntimeException(NO_TYPE_FOUND.apply(PathFormat.class, source)));
	}
    // @formatter:on

}
