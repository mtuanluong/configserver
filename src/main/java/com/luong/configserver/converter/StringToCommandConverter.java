package com.luong.configserver.converter;

import static com.luong.common.functional.MessageFunctions.NO_TYPE_FOUND;

import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.luong.configserver.user.request.model.Command;

@Component
@ConfigurationPropertiesBinding
public class StringToCommandConverter implements Converter<String, Command> {

	// @formatter:off
	@Override
	public Command convert(String source) {
		return Command.find(source)
			.orElseThrow(() -> new RuntimeException(NO_TYPE_FOUND.apply(Command.class, source)));
	}
    // @formatter:on

}
