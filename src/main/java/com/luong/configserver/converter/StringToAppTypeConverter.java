package com.luong.configserver.converter;

import static com.luong.common.functional.MessageFunctions.NO_TYPE_FOUND;

import java.util.stream.Stream;

import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.luong.configserver.config.model.ArtifactType;

@Component
@ConfigurationPropertiesBinding
public class StringToAppTypeConverter implements Converter<String, ArtifactType> {

	// @formatter:off
	@Override
	public ArtifactType convert(String source) {
		return Stream.of(ArtifactType.values())
			.filter(a -> a.getValue().equals(source))
			.findFirst()
			.orElseThrow(() -> new RuntimeException(NO_TYPE_FOUND.apply(ArtifactType.class, source)));
	}
    // @formatter:on

}
