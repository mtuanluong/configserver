package com.luong.configserver.converter;

import static com.luong.common.functional.MessageFunctions.NO_TYPE_FOUND;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@ConfigurationPropertiesBinding
public class StringToPathConverter implements Converter<String, Path> {

	// @formatter:off
	@Override
	public Path convert(String source) {
		return Optional.of(source)
			.map(Paths::get)
			.filter(Files::exists)
			.orElseThrow(() -> new RuntimeException(NO_TYPE_FOUND.apply(Path.class, source)));
	}
    // @formatter:on

}
