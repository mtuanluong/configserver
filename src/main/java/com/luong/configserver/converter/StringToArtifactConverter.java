package com.luong.configserver.converter;

import static com.luong.common.functional.MessageFunctions.NO_TYPE_FOUND;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.luong.configserver.config.ApplicationConfiguration;
import com.luong.configserver.config.model.Artifact;

@Component
@ConfigurationPropertiesBinding
public class StringToArtifactConverter implements Converter<String, Artifact> {

	// Autowired works here because this converter will be used for rest controller
	// at run time, not at start time
	// as other converters
	private @Autowired ApplicationConfiguration configuration;

	// @formatter:off
	@Override
	public Artifact convert(String source) {
		return configuration.findArtifact(source)
			.orElseThrow(() -> new RuntimeException(NO_TYPE_FOUND.apply(Artifact.class, source)));
	}
    // @formatter:on

}
