package com.luong.configserver.process;

import static com.luong.configserver.config.model.ArtifactType.Backend;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.luong.common.process.StreamGobbler;
import com.luong.configserver.config.ApplicationConfiguration;
import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.context.ContextDataService;
import com.luong.configserver.context.model.FrontendProjectInfo;
import com.luong.configserver.context.model.GradleProjInfo;

import lombok.SneakyThrows;

@Service
public class ProcessService {

	private Set<String> jarFileSet;

	private @Autowired ContextDataService contextDataService;
	private @Autowired ApplicationConfiguration configuration;

	// @formatter:off
//	@PostConstruct
	public void postConstruct() {
		jarFileSet = configuration.findArtifacts(Optional.of(Backend))
			.stream()
			.map(artifact -> 
				String.format("%s-%s.jar", 
					contextDataService.findBackendInfo(artifact).get().getName(), 
					contextDataService.findBackendInfo(artifact).get().getVersion()))
			.collect(Collectors.toSet());
	}
	
	public CompletableFuture<GradleProjInfo> retrieveGradleProjInfo(Artifact artifact) {
		return null;
//		return CompletableFuture.completedFuture(retrieveProperties(
//			commandMappers.stream()
//				.filter(m -> m.getCondition().equals(artifact.getAppType()))
//				.findFirst()
//				.map(m -> m.convert(SpringAppRequestObj.builder().artifact(artifact).commandType(GetProperties).build()))
//				.get(), 
//			artifact.getPath()));
	}
	
	public CompletableFuture<FrontendProjectInfo> retrieveFrontendProjInfo(Artifact artifact) {
		// Too much efforts
		// Currently nothing here
		
		return CompletableFuture.completedFuture(null);
	}

	@SneakyThrows
	private GradleProjInfo retrieveProperties(String command, Path folder) {
		Process process = new ProcessBuilder()
			.command("cmd.exe", "/c", command)
			.directory(folder.toFile())
			.start();
		Properties props = new Properties();
		props.load(process.getInputStream());
		process.waitFor();
		return GradleProjInfo.builder()
			.name(props.getProperty("name"))
			.version(props.getProperty("version"))
			.build();
	}
	
	public void openUrl(String url) throws IOException {
		String os = contextDataService.getOperatingSystemName().get().toLowerCase();
		Runtime rt = Runtime.getRuntime();
		if (os.indexOf( "win" ) >= 0) {
	        // this doesn't support showing urls in the form of "page.html#nameLink" 
	        rt.exec( "rundll32 url.dll,FileProtocolHandler " + url);
	    } else if (os.indexOf( "mac" ) >= 0) {
	        rt.exec( "open " + url);
        } else if (os.indexOf( "nix") >=0 || os.indexOf( "nux") >=0) {
	        // Do a best guess on unix until we get a platform independent way
	        // Build a list of browsers to try, in this order.
	        String[] browsers = {"epiphany", "firefox", "mozilla", "konqueror",
	       			             "netscape","opera","links","lynx"};

	        // Build a command string which looks like "browser1 "url" || browser2 "url" ||..."
	        StringBuffer cmd = new StringBuffer();
	     for (int i=0; i<browsers.length; i++)
	    	cmd.append( (i==0  ? "" : " || " ) + browsers[i] +" \"" + url + "\" ");

	     	rt.exec(new String[] { "sh", "-c", cmd.toString() });
           } else {
                return;
           }
	}

	@Async
	public CompletableFuture<Process> startProgram(String command, Path folder) throws IOException, InterruptedException {
		new ProcessBuilder()
		// Fix me
			.command("cmd.exe", "/c", "C:/Software/Platform/nodejs/" + command)
			.inheritIO()
			.directory(folder.toFile())
			.start()
			.waitFor();
		return CompletableFuture.completedFuture(null);
	}
	
	@SneakyThrows
	public void cleanUpProcesses() {
		postConstruct();
		Process jpsProcess = new ProcessBuilder()
				.command("cmd.exe", "/c", "jps")
				.start();
		
		new StreamGobbler(jpsProcess, this::handleInput).run();
		
	}
	
	@SneakyThrows
	private void handleInput(String str) {
		String[] arr = str.split(" ");
		if (jarFileSet.contains(arr[1])) {
			Process process = new ProcessBuilder()
				.command("cmd.exe", "/c", "taskkill /F /PID " + arr[0])
				.inheritIO()
				.start();
			System.out.println("Trying to kill process with PID = " + arr[0]);
			process.waitFor();
		}
	}
	// @formatter:on
}
