package com.luong.configserver.listener;

import static com.luong.common.functional.MessageFunctions.PRINT_CURRENT_THREAD_NAME;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import com.luong.configserver.context.provider.DataGather;
import com.luong.configserver.event.ContextDataReadyEvent;

import lombok.Setter;

@Component
public class ApplicationStartupListener implements ApplicationRunner {

	private @Resource(name = "initialDataGather") DataGather initialDataGather;
	private @Resource(name = "actualDataGather") DataGather actualDataGather;
	private @Autowired ApplicationEventPublisher publisher;
	@Value("${data.gather.enabled}")
	private @Setter boolean dataGatherEnabled;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		PRINT_CURRENT_THREAD_NAME.accept(this);
		initialDataGather.run();
		if (dataGatherEnabled) {
			actualDataGather.run();
		}
		publisher.publishEvent(new ContextDataReadyEvent(this));
	}

}
