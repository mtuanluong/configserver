package com.luong.configserver.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

import com.luong.configserver.process.ProcessService;

@Component
public class ApplicationShutdownListener implements ApplicationListener<ContextClosedEvent> {

	private @Autowired ProcessService processService;

	@Override
	public void onApplicationEvent(ContextClosedEvent event) {
		processService.cleanUpProcesses();
	}

}
