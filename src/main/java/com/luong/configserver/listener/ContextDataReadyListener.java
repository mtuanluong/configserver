package com.luong.configserver.listener;

import static com.luong.common.logging.LoggerNameComm.Root;

import java.util.stream.Stream;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.luong.common.logging.Logger;
import com.luong.common.logging.MessageLogEntry;
import com.luong.configserver.context.ContextDataService;
import com.luong.configserver.event.ContextDataReadyEvent;

@Component
public class ContextDataReadyListener implements ApplicationListener<ContextDataReadyEvent> {

	private @Autowired ContextDataService contextDataService;
	private @Autowired Logger logger;

	// @formatter:off
	@Override
	public void onApplicationEvent(ContextDataReadyEvent event) {
		Stream.of(
			Pair.of("+++ Backend info +++", contextDataService.getAllBackends()),
			Pair.of("+++ Frontend info +++", contextDataService.getAllFrontends()),
			Pair.of("+++ Library info +++", contextDataService.getAllLibraries())
		)
		.forEach(p -> {
			logger.info(Root, MessageLogEntry.builder()
				.message(p.getLeft())
				.build());
			p.getRight().entrySet()
				.forEach(e -> logger.info(Root, MessageLogEntry.builder()
					.message(e.toString())
					.build()));
		});
	}
    // @formatter:on

}
