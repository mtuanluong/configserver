package com.luong.configserver.event;

import org.springframework.context.ApplicationEvent;

@SuppressWarnings("serial")
public class ContextDataReadyEvent extends ApplicationEvent {

	public ContextDataReadyEvent(Object source) {
		super(source);
	}

}
