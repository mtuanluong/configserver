package com.luong.configserver.user.response;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class PathResponse implements Response {

	private @NonNull String artifactName;

	private @NonNull String path;

}
