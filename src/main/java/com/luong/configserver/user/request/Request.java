package com.luong.configserver.user.request;

import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.user.request.model.ArtifactCharacter;
import com.luong.configserver.user.request.model.Command;

public interface Request {

	Artifact getArtifact();

	Command getCommand();

	ArtifactCharacter getArtifactCharacter();

}
