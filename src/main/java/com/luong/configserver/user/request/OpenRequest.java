package com.luong.configserver.user.request;

import static com.luong.configserver.user.request.model.Command.Open;

import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.user.request.model.ArtifactCharacter;
import com.luong.configserver.user.request.model.Command;
import com.luong.configserver.user.request.model.DefaultArtifactCharacter;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class OpenRequest implements Request {

	private @NonNull Artifact artifact;

	private final Command command = Open;

	private final ArtifactCharacter artifactCharacter = DefaultArtifactCharacter.builder().build();

}
