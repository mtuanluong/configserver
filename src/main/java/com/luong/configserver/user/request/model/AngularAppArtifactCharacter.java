package com.luong.configserver.user.request.model;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class AngularAppArtifactCharacter implements ArtifactCharacter {

	private boolean prod;

	private int port;

	private boolean openInBrowser;

	private String useHttpServer;

}
