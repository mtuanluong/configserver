package com.luong.configserver.user.request.model;

import static com.luong.common.path.PathFormat.Cygwin;

import java.nio.file.Path;

import com.luong.common.path.PathFormat;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class GradleProjArtifactCharacter implements ArtifactCharacter {

	private @Builder.Default boolean useWrapper = true;

	private Path useJava;

	private @Builder.Default PathFormat pathFormat = Cygwin;

	private @Builder.Default boolean skipTest = true;

	private boolean publish;

}
