package com.luong.configserver.user.request.model;

import java.util.Optional;
import java.util.stream.Stream;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Command {

	// @formatter:off
	Build("build"),
	Open("open"),
	Properties("properties"),
	Run("run"),
	Start("start"),
	Stop("stop");
	
	private final @NonNull @Getter String name;

	public static Optional<Command> find(String str) {
		return Stream.of(Command.values())
			.filter(c -> c.getName().equals(str))
			.findFirst();
	}
    // @formatter:on

}
