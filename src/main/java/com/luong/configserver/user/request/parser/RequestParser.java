package com.luong.configserver.user.request.parser;

import java.lang.reflect.Field;
import java.nio.file.Path;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.luong.common.functional.Predicates;
import com.luong.configserver.context.ContextDataService;
import com.luong.configserver.context.model.FrontendProjectInfo;
import com.luong.configserver.user.action.UserActionManager;
import com.luong.configserver.user.request.CommandRequest;
import com.luong.configserver.user.request.Request;
import com.luong.configserver.user.request.model.AngularAppArtifactCharacter;
import com.luong.configserver.user.request.model.AngularAppArtifactCharacter.AngularAppArtifactCharacterBuilder;
import com.luong.configserver.user.request.model.ArtifactCharacter;
import com.luong.configserver.user.request.model.GradleProjArtifactCharacter;
import com.luong.configserver.user.request.model.GradleProjArtifactCharacter.GradleProjArtifactCharacterBuilder;
import com.luong.configserver.user.request.model.JarsExecutableArtifactCharacter;
import com.luong.configserver.user.request.model.JarsExecutableArtifactCharacter.JarsExecutableArtifactCharacterBuilder;
import com.luong.configserver.user.request.model.SpringAppArtifactCharacter;
import com.luong.configserver.user.request.model.SpringAppArtifactCharacter.SpringAppArtifactCharacterBuilder;
import com.luong.configserver.user.request.model.WindowsExecutableArtifactCharacter;
import com.luong.configserver.user.request.model.WindowsExecutableArtifactCharacter.WindowsExecutableArtifactCharacterBuilder;

import lombok.SneakyThrows;

@Component
public class RequestParser {

	private Map<Class<? extends ArtifactCharacter>, Set<String>> requestClassFieldNames;
	private Map<Class<? extends ArtifactCharacter>, Function<RequestParserObj, ? extends ArtifactCharacter>> commandStrategiesMap;

	private @Autowired ContextDataService contextDataService;
	private @Autowired UserActionManager userActionManager;

	// @formatter:off
	@PostConstruct
	public void postConstruct() {
		Set<Pair<? extends Class<? extends ArtifactCharacter>, 
			? extends Function<RequestParserObj, ? extends ArtifactCharacter>>> set = Stream.of(
			Pair.of(AngularAppArtifactCharacter.class, (Function<RequestParserObj, AngularAppArtifactCharacter>) this::mapAngularRequest),
			Pair.of(GradleProjArtifactCharacter.class, (Function<RequestParserObj, GradleProjArtifactCharacter>)this::mapGradleRequest),
			Pair.of(SpringAppArtifactCharacter.class, (Function<RequestParserObj, SpringAppArtifactCharacter>) this::mapSpringRequest),
			Pair.of(WindowsExecutableArtifactCharacter.class, (Function<RequestParserObj, WindowsExecutableArtifactCharacter>) this::mapWindowsExecutableRequest),
			Pair.of(JarsExecutableArtifactCharacter.class, (Function<RequestParserObj, JarsExecutableArtifactCharacter>) this::mapJarsExecutableRequest)
		).collect(Collectors.toSet());

		requestClassFieldNames = set.stream()
			.map(Pair::getLeft)
			.collect(Collectors.toMap(
    			Function.identity(), 
    			o -> FieldUtils.getAllFieldsList(o).stream()
    				.map(Field::getName)
    				.collect(Collectors.toSet())));

		commandStrategiesMap = set.stream().collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
	}

	public Request parse(RequestParserObj requestParserObj) {
		// Get all field names of obj that not null
		Set<Object> setVariables = FieldUtils.getAllFieldsList(RequestParserObj.class)
			.stream()
			.map(f -> getFieldValue(f, requestParserObj))
			.filter(Objects::nonNull)
			.collect(Collectors.toSet());

		// Try to match here
		// Still doesn't work since the setVariables contain only declared field, not from it's parent...
//		Class<? extends ArtifactCharacter> subArtifactCharacter = requestClassFieldNames.entrySet()
//			.stream()
//			.filter(e -> e.getValue().containsAll(setVariables))
//			.map(Map.Entry::getKey)
//			.findFirst()
//			.orElseThrow(() -> new RuntimeException(MessageFunctions.NO_TYPE_FOUND.apply(ArtifactCharacter.class, "")));
		
		// TODO: It should be more elegant with reflection as above if it works
		// Not clean here
		switch (requestParserObj.getArtifact().getArtifactType()) {
		case Backend:
		case Library:
			try {
				CommandRequest commandRequest = new CommandRequest(
					requestParserObj.getArtifact(),
					requestParserObj.getCommand(),
					commandStrategiesMap.get(GradleProjArtifactCharacter.class).apply(requestParserObj));
				userActionManager.handle(commandRequest);
				return commandRequest;
			} catch (Exception e) {
				return new CommandRequest(
					requestParserObj.getArtifact(),
					requestParserObj.getCommand(),
					commandStrategiesMap.get(SpringAppArtifactCharacter.class).apply(requestParserObj));
			}
		case Frontend:
			return new CommandRequest(
				requestParserObj.getArtifact(),
				requestParserObj.getCommand(),
				commandStrategiesMap.get(AngularAppArtifactCharacter.class).apply(requestParserObj));
		case WindowsExecutable:
			return new CommandRequest(
				requestParserObj.getArtifact(),
				requestParserObj.getCommand(),
				commandStrategiesMap.get(WindowsExecutableArtifactCharacter.class).apply(requestParserObj));
		case JarsExecutable:
			return new CommandRequest(
					requestParserObj.getArtifact(),
					requestParserObj.getCommand(),
					commandStrategiesMap.get(JarsExecutableArtifactCharacter.class).apply(requestParserObj));
		default:
			throw new UnsupportedOperationException("Not yet implemented");
		}
	}

	@SneakyThrows
	private Object getFieldValue(Field field, Object obj) {
		return FieldUtils.readField(field, obj, true);
	}

	private AngularAppArtifactCharacter mapAngularRequest(RequestParserObj requestParserObj) {
		AngularAppArtifactCharacterBuilder builder = AngularAppArtifactCharacter.builder();
		Optional.ofNullable(requestParserObj.getProd())
			.ifPresent(builder::prod);
    	contextDataService.findFrontendInfo(requestParserObj.getArtifact())
    		.map(FrontendProjectInfo::getPort)
    		.ifPresent(builder::port);
    	Optional.ofNullable(requestParserObj.getOpenInBrowser())
    		.ifPresent(builder::openInBrowser);
    	Optional.ofNullable(requestParserObj.getUseHttpServer())
    		.filter(Boolean::valueOf)
    		.ifPresent(b -> builder.useHttpServer(contextDataService
    			.findFrontendInfo(requestParserObj.getArtifact())
    			.map(FrontendProjectInfo::getDistFolderRelPath)
    			.map(Path::toString)
    			.get()));
				
		return builder.build();
	}

	private GradleProjArtifactCharacter mapGradleRequest(RequestParserObj requestParserObj) {
		GradleProjArtifactCharacterBuilder builder = GradleProjArtifactCharacter.builder();
    	Optional.ofNullable(requestParserObj.getPathFormat())
    		.ifPresent(builder::pathFormat);
    	Optional.ofNullable(requestParserObj.getUseWrapper())
    		.ifPresent(builder::useWrapper);
    	Optional.ofNullable(requestParserObj.getUseJava())
    		.ifPresent(builder::useJava);
    	Optional.ofNullable(requestParserObj.getSkipTest())
    		.ifPresent(builder::skipTest);
    	Optional.ofNullable(requestParserObj.getPublish())
    		.ifPresent(builder::publish);
    	
		return builder.build();
	}

	private SpringAppArtifactCharacter mapSpringRequest(RequestParserObj requestParserObj) {
		SpringAppArtifactCharacterBuilder builder = SpringAppArtifactCharacter.builder();
		Optional.ofNullable(requestParserObj.getPathFormat())
			.ifPresent(builder::pathFormat);
    	Optional.ofNullable(requestParserObj.getSpringProfile())
    		.ifPresent(builder::springProfile);
    	Optional.ofNullable(requestParserObj.getOtherSpringArgs())
    		.filter(Predicates.negate(Map::isEmpty))
    		.ifPresent(builder::otherSpringArgs);
    	Optional.ofNullable(requestParserObj.getQuote())
    		.ifPresent(builder::quotePath);
		
		return builder.build();
	}
	
	private WindowsExecutableArtifactCharacter mapWindowsExecutableRequest(RequestParserObj requestParserObj) {
		WindowsExecutableArtifactCharacterBuilder builder = WindowsExecutableArtifactCharacter.builder();
		Optional.ofNullable(requestParserObj.getPathFormat())
			.ifPresent(builder::pathFormat);
	
		return builder.build();
	}

	private JarsExecutableArtifactCharacter mapJarsExecutableRequest(RequestParserObj requestParserObj) {
		JarsExecutableArtifactCharacterBuilder builder = JarsExecutableArtifactCharacter.builder();
		Optional.ofNullable(requestParserObj.getPathFormat())
			.ifPresent(builder::pathFormat);
		
		return builder.build();
	}
    // @formatter:on
}
