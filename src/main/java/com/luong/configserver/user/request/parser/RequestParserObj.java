package com.luong.configserver.user.request.parser;

import java.nio.file.Path;
import java.util.Map;

import com.luong.common.path.PathFormat;
import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.user.request.model.Command;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class RequestParserObj {

	private Artifact artifact;
	private Command command;

	// Spring
	private PathFormat pathFormat;
	private String springProfile;
	private Map<String, String> otherSpringArgs;
	private Boolean quote;

	// Angular
	private Boolean prod;
	private Integer port;
	private Boolean openInBrowser;
	private Boolean useHttpServer;

	// Gradle
	private Boolean useWrapper;
	private Path useJava;
	private Boolean skipTest;
	private Boolean publish;

}
