package com.luong.configserver.user.request;

import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.user.request.model.ArtifactCharacter;
import com.luong.configserver.user.request.model.Command;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class StartAvpRequest implements Request {

	private @NonNull Artifact artifact;

	private @NonNull Command command;

	private @NonNull ArtifactCharacter artifactCharacter;

}
