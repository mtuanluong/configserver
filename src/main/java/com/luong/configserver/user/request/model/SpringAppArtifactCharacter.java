package com.luong.configserver.user.request.model;

import java.util.HashMap;
import java.util.Map;

import com.luong.common.path.PathFormat;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class SpringAppArtifactCharacter implements ArtifactCharacter {

	private @Builder.Default PathFormat pathFormat = PathFormat.Windows;

	private @Builder.Default boolean quotePath = true;

	private String springProfile;

	private @Builder.Default Map<String, String> otherSpringArgs = new HashMap<>();

}
