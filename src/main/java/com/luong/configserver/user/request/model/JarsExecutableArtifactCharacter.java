package com.luong.configserver.user.request.model;

import com.luong.common.path.PathFormat;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class JarsExecutableArtifactCharacter implements ArtifactCharacter {

	private @Builder.Default PathFormat pathFormat = PathFormat.Cygwin;

	private @Builder.Default boolean quotePath = true;

}
