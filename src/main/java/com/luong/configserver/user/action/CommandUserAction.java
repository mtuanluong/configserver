package com.luong.configserver.user.action;

import static com.luong.common.functional.MessageFunctions.UNSUPPORTED_OPERATION;
import static com.luong.configserver.config.model.ArtifactType.Backend;
import static com.luong.configserver.config.model.ArtifactType.Frontend;
import static com.luong.configserver.config.model.ArtifactType.JarsExecutable;
import static com.luong.configserver.config.model.ArtifactType.Library;
import static com.luong.configserver.config.model.ArtifactType.WindowsExecutable;
import static com.luong.configserver.user.request.model.Command.Build;
import static com.luong.configserver.user.request.model.Command.Properties;
import static com.luong.configserver.user.request.model.Command.Run;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.luong.configserver.cmd.builder.CmdCommandBuilder;
import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.config.model.ArtifactType;
import com.luong.configserver.user.request.CommandRequest;
import com.luong.configserver.user.request.model.Command;
import com.luong.configserver.user.response.CommandResponse;
import com.luong.configserver.user.response.Response;

import lombok.Getter;

@Component
public class CommandUserAction implements UserAction<CommandRequest> {

	private final @Getter Class<CommandRequest> requestType = CommandRequest.class;
	private Map<ArtifactType, Set<Command>> possibleCommandForArtifactMap;
	private Map<Command, BiFunction<CmdCommandBuilder<?>, CommandRequest, String>> strategies;

	private @Autowired Set<CmdCommandBuilder<?>> cmdCommandBuilders;

	// @formatter:off
	@PostConstruct
	public void postConstruct() {
		possibleCommandForArtifactMap = Stream.of(
			Pair.of(Backend, Stream.of(Build, Run, Properties).collect(Collectors.toSet())),
			Pair.of(Frontend, Stream.of(Build, Run).collect(Collectors.toSet())),
			Pair.of(Library, Stream.of(Build, Run, Properties).collect(Collectors.toSet())),
			Pair.of(WindowsExecutable, Stream.of(Run).collect(Collectors.toSet())),
			Pair.of(JarsExecutable, Stream.of(Run).collect(Collectors.toSet()))
		).collect(Collectors.toMap(Pair::getLeft, Pair::getRight));

		strategies = Stream.of(
			Pair.of(Build, (BiFunction<CmdCommandBuilder<?>, CommandRequest, String>) (builder, request) -> builder.commandBuild(request)),
			Pair.of(Run, (BiFunction<CmdCommandBuilder<?>, CommandRequest, String>) (builder, request) -> builder.commandRun(request)),
			Pair.of(Properties, (BiFunction<CmdCommandBuilder<?>, CommandRequest, String>) (builder, request) -> builder.commandProperties(request))
		).collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
	}

	@Override
	public Optional<? extends Response> fire(CommandRequest request) {
		// To know which command is possible
		ArtifactType artifactType = Optional.of(request)
			.map(CommandRequest::getArtifact)
			.map(Artifact::getArtifactType)
			.get();

		if (!possibleCommandForArtifactMap.containsKey(artifactType) ||
				!possibleCommandForArtifactMap.get(artifactType).contains(request.getCommand())) {
			throw new RuntimeException(UNSUPPORTED_OPERATION.apply(request.getCommand(), request.getArtifact().getName()));
		}
		
		return cmdCommandBuilders.stream()
			.filter(b -> b.getType().equals(request.getArtifactCharacter().getClass()))
			.map(b -> strategies.get(request.getCommand()).apply(b, request))
			.map(str -> new CommandResponse(request.getArtifact().getName(), str))
			.findFirst();
	}
    // @formatter:on

}
