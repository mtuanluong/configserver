package com.luong.configserver.user.action;

import static com.luong.configserver.config.model.ArtifactType.WindowsExecutable;

import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.luong.configserver.config.model.ArtifactType;
import com.luong.configserver.process.ProcessService;
import com.luong.configserver.user.request.OpenRequest;
import com.luong.configserver.user.response.Response;

import lombok.Getter;
import lombok.SneakyThrows;

@Component
public class OpenUserAction implements UserAction<OpenRequest> {

	private final @Getter Class<OpenRequest> requestType = OpenRequest.class;
	private Map<ArtifactType, Consumer<Path>> strategies;

	private @Autowired ProcessService processService;

	// @formatter:off
	@PostConstruct
	public void postConstruct() {
		strategies = Stream.of(
			Pair.of(WindowsExecutable, (Consumer<Path>) this::openParentFolder)
		).collect(Collectors.toMap(Pair::getLeft, Pair::getRight));
	}
	
	@SneakyThrows
	private void openDirectly(Path path) {
		processService.openUrl(path.toString());
	}
	
	private void openParentFolder(Path path) {
		openDirectly(path.getParent());
	}

	@Override
	public Optional<? extends Response> fire(OpenRequest request) {
		strategies.getOrDefault(
			request.getArtifact().getArtifactType(),
			this::openDirectly)
			.accept(request.getArtifact().getPath());
			
		return Optional.empty();
	}
    // @formatter:on

}
