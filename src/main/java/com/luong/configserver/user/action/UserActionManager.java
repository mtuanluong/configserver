package com.luong.configserver.user.action;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.luong.common.functional.Optionals;
import com.luong.configserver.user.request.Request;
import com.luong.configserver.user.response.Response;

@Component
@SuppressWarnings({ "rawtypes", "unchecked" })
public class UserActionManager {

	private @Autowired Set<UserAction> userActions;

	// @formatter:off
	public Optional<Response> handle(Request request) {
		return userActions.stream()
			.filter(u -> u.getRequestType().equals(request.getClass()))
			.map(u -> u.fire(request))
			.flatMap(Optionals::stream)
			.findFirst();
	}
    // @formatter:on
}
