package com.luong.configserver.user.action;

import static com.luong.common.path.PathFormat.Windows;
import static com.luong.configserver.user.request.model.Command.Stop;

import java.nio.file.Path;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.luong.configserver.config.ApplicationConfiguration;
import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.context.ContextDataService;
import com.luong.configserver.process.ProcessService;
import com.luong.configserver.user.request.CommandRequest;
import com.luong.configserver.user.request.StartAvpRequest;
import com.luong.configserver.user.request.model.AngularAppArtifactCharacter;
import com.luong.configserver.user.request.model.Command;
import com.luong.configserver.user.request.model.SpringAppArtifactCharacter;
import com.luong.configserver.user.request.model.WindowsExecutableArtifactCharacter;
import com.luong.configserver.user.response.CommandResponse;
import com.luong.configserver.user.response.Response;

import lombok.Getter;
import lombok.SneakyThrows;

@Component
public class StartAvpUserAction implements UserAction<StartAvpRequest> {

	private final @Getter Class<StartAvpRequest> requestType = StartAvpRequest.class;

	private @Autowired UserActionManager userActionManager;
	private @Autowired ApplicationConfiguration configuration;
	private @Autowired ProcessService processService;
	private @Autowired ContextDataService contextDataService;

	// @formatter:off
	@Override
	public Optional<? extends Response> fire(StartAvpRequest request) {
		if (request.getCommand().equals(Stop)) {
			processService.cleanUpProcesses();
			return Optional.empty();
		}
		
		// TODO: Flag here to prevent multiple startup
//		startBroker();
		/*
		startSpringProgram(
			configuration.findArtifact("sysinit").get(), 
			SpringAppArtifactCharacter.class.cast(request.getArtifactCharacter()).getSpringProfile());
		
		// display still not here
		Stream.of("airbridge", "carman", "gatecontrol", "locator", "manspace", "mobridge", "objgen", "phadmin",
			"pushnotify", "useradmin")
			.map(configuration::findArtifact)
			.flatMap(Optionals::stream)
			.forEach(a -> startSpringProgram(
				a, 
				SpringAppArtifactCharacter.class.cast(request.getArtifactCharacter()).getSpringProfile()));
*/
		
		configuration.findArtifact("carmanui")
			.ifPresent(this::startAngularProgram);
		return Optional.empty();
	}
	
	private void startBroker() {
		CommandRequest commandRequest = new CommandRequest(
			configuration.findArtifact("broker").get(),
			Command.Run,
			WindowsExecutableArtifactCharacter.builder()
				.pathFormat(Windows)
				.quotePath(false)
				.build());

		userActionManager.handle(commandRequest)
			.map(CommandResponse.class::cast)
			.map(CommandResponse::getCommand)
			.ifPresent(command -> startProgram(command, commandRequest.getArtifact().getPath().getParent()));
	}
	
	private void startSpringProgram(Artifact artifact, String springProfile) {
		CommandRequest commandRequest = new CommandRequest(
				artifact,
				Command.Run,
				SpringAppArtifactCharacter.builder()
					.springProfile(springProfile)
					.quotePath(false)
					.build()
			);

			userActionManager.handle(commandRequest)
				.map(CommandResponse.class::cast)
				.map(CommandResponse::getCommand)
				.ifPresent(command -> startProgram(command, commandRequest.getArtifact().getPath()));
	}
	
	private void startAngularProgram(Artifact artifact) {
		CommandRequest commandRequest = new CommandRequest(
			artifact,
			Command.Run,
			AngularAppArtifactCharacter.builder()
				.openInBrowser(true)
				.prod(true)
				.port(contextDataService.findFrontendInfo(artifact).get().getPort())
				.build()
		);
	
		userActionManager.handle(commandRequest)
    		.map(CommandResponse.class::cast)
    		.map(CommandResponse::getCommand)
    		.ifPresent(command -> startProgram(command, commandRequest.getArtifact().getPath()));
	}

	@SneakyThrows
	private void startProgram(String command, Path path) {
		processService.startProgram(command, path);
	}
    // @formatter:on

}
