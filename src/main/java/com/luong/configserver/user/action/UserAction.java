package com.luong.configserver.user.action;

import java.util.Optional;

import com.luong.configserver.user.request.Request;
import com.luong.configserver.user.response.Response;

public interface UserAction<R extends Request> {

	Class<R> getRequestType();

	Optional<? extends Response> fire(R request);

}
