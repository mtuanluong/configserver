package com.luong.configserver.user.controller;

import static com.luong.common.constant.Variables.INF_SERVER_HOME;
import static com.luong.common.constant.WebConstants.API_MAPPING;
import static com.luong.common.path.PathFormat.Windows;
import static com.luong.configserver.config.model.ArtifactType.Backend;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.luong.common.functional.Optionals;
import com.luong.common.path.PathFormat;
import com.luong.common.path.PathService;
import com.luong.configserver.config.ApplicationConfiguration;
import com.luong.configserver.config.model.Artifact;
import com.luong.configserver.config.model.ArtifactType;
import com.luong.configserver.user.action.UserActionManager;
import com.luong.configserver.user.request.OpenRequest;
import com.luong.configserver.user.request.StartAvpRequest;
import com.luong.configserver.user.request.model.Command;
import com.luong.configserver.user.request.model.SpringAppArtifactCharacter;
import com.luong.configserver.user.request.model.SpringAppArtifactCharacter.SpringAppArtifactCharacterBuilder;
import com.luong.configserver.user.request.parser.RequestParser;
import com.luong.configserver.user.request.parser.RequestParserObj;
import com.luong.configserver.user.request.parser.RequestParserObj.RequestParserObjBuilder;
import com.luong.configserver.user.response.CommandResponse;

@RestController
@RequestMapping(API_MAPPING)
public class Controller {

	private @Autowired ApplicationConfiguration configuration;
	private @Autowired RequestParser requestParser;
	private @Autowired UserActionManager userActionManager;
	private @Autowired PathService pathService;

	// @formatter:off
	@GetMapping(value = "/path")
	public Map<String, String> path(
			@RequestParam("artifactType") Optional<ArtifactType> artifactTypeOpt,
			@RequestParam("pathFormat") Optional<PathFormat> pathFormatOpt) {

		return configuration.findPaths(artifactTypeOpt, pathFormatOpt);
	}

	@GetMapping("command")
	public ResponseEntity<CommandResponse> command(
		@RequestParam("artifact") Artifact artifact,
		@RequestParam("command") Command command,
		
		@RequestParam("pathFormat") Optional<PathFormat> pathFormat,
		@RequestParam("springProfile") Optional<String> springProfile,
		@RequestParam("quote") Optional<Boolean> quote,
		
		@RequestParam("prod") Optional<Boolean> prod,
		@RequestParam("port") Optional<Integer> port,
		@RequestParam("openInBrowser") Optional<Boolean> openInBrowser,
		@RequestParam("useHttpServer") Optional<Boolean> useHttpServer,
		
		@RequestParam("useWrapper") Optional<Boolean> useWrapper,
		@RequestParam("useJava") Optional<String> useJava,
		@RequestParam("skipTest") Optional<Boolean> skipTest,
		@RequestParam("publish") Optional<Boolean> publish,
		@RequestParam("debug") Optional<Boolean> debug) {
			
		RequestParserObjBuilder builder = RequestParserObj.builder()
			.artifact(artifact)
			.command(command);
		
			Map<String, String> otherSpringArgs = new HashMap<>();
			
			pathFormat.ifPresent(builder::pathFormat);
			springProfile.ifPresent(builder::springProfile);
			quote.ifPresent(builder::quote);
			Stream.of(
					configuration.findArtifact("sysinit"),
					configuration.findArtifact("procman"))
				.flatMap(Optionals::stream)
				.filter(artifact::equals)
				.map(a -> configuration.findArtifact("infserver"))
				.flatMap(Optionals::stream)
				.map(Artifact::getPath)
				.findFirst()
				.ifPresent(p -> otherSpringArgs.put(INF_SERVER_HOME, pathService.convert(p, Windows, true)));
			
			prod.ifPresent(builder::prod);
			port.ifPresent(builder::port);
			openInBrowser.ifPresent(builder::openInBrowser);
			useHttpServer.ifPresent(builder::useHttpServer);

			useWrapper.ifPresent(builder::useWrapper);
			useJava
				.flatMap(configuration::findArtifact)
				.map(a -> a.getPath())
				.ifPresent(builder::useJava);
			skipTest.ifPresent(builder::skipTest);
			publish.ifPresent(builder::publish);
			debug.ifPresent(d -> {
				otherSpringArgs.put("debug", d.toString());
			});

			builder.otherSpringArgs(otherSpringArgs);
		
		return ResponseEntity.of(Optional.of(builder.build())
			.map(requestParser::parse)
			.flatMap(userActionManager::handle)
			.map(CommandResponse.class::cast));
	}
	
	@GetMapping("/commands")
	public List<String> commands() {
		
		return Stream.of(Command.values())
				.map(c -> c.getName())
				.sorted()
				.collect(Collectors.toList());
	}

	@GetMapping("/artifactType")
	public ResponseEntity<List<String>> artifactType(
		@RequestParam("artifact") Optional<Artifact> artifact) {
		
		return ResponseEntity.ok((artifact
			.map(Artifact::getArtifactType)
			.map(ArtifactType::getValue)
			.map(Arrays::asList)
			.orElse(ArtifactType.findAllValues())));
	}

	@GetMapping("/open")
	public ResponseEntity<HttpStatus> open(@RequestParam("artifact") Artifact artifact) {
		userActionManager.handle(new OpenRequest(artifact));
		
		return ResponseEntity.ok().build();
	}

	@GetMapping("/avp")
	public ResponseEntity<HttpStatus> avp(
		// command = <start | stop>
		@RequestParam("command") Command command,
		@RequestParam("springProfile") Optional<String> springProfile) {
		
		SpringAppArtifactCharacterBuilder builder = SpringAppArtifactCharacter.builder();
		springProfile.ifPresent(builder::springProfile);
		userActionManager.handle(new StartAvpRequest(
			new Artifact("dummy", Paths.get("not_important"), Backend),
			command,
			builder.build()));

		return ResponseEntity.ok().build();
	}
    // @formatter:on

}
